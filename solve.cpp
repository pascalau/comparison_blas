#include<iostream>
#include<chrono>
#include<exception>
#include<random>

extern "C"{
extern int dgesv_(int*,int*,double*,int*,int*,double*,int*,int*);
}


int main(int argc, char** argv){

    //std::cout << openblas_get_parallel();

    int n;

    try{
        n = std::stoi(argv[1]);
    }
    catch(std::exception& e){
        std::cerr << e.what() << std::endl;
        return 0;
    }

    std::default_random_engine generator;
    std::uniform_real_distribution<double> distribution(0.0,1.0);

    double* A = new double[n*n];
    double* b = new double[n];
    int* ipiv = new int[n];

    int info,nrhs,lda,ldb;
    info = nrhs = 1;
    lda = ldb = n;


    for(int i = 0; i < n*n; i++){
        *(A+i)= distribution(generator);
    }

    for(int i = 0; i < n; i++){
        *(b+i) = distribution(generator);
    }



    std::cout << "Beginning to solve matrix" << std::endl;

    auto start = std::chrono::high_resolution_clock::now();

    dgesv_(&n,&nrhs,A,&lda,ipiv,b,&ldb,&info);

    auto end = std::chrono::high_resolution_clock::now();

    if(info != 0){
        std::cout << "Error from lapack: " << info << std::endl;
        return 0;
    }

    auto time = std::chrono::duration_cast<std::chrono::duration<double>> (end-start);

    std::cout << "C++ used " << time.count() << " seconds." << std::endl;

    delete [] A;
    delete [] b;
    delete [] ipiv;

};
