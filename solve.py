#!/usr/bin/env python3

import numpy as np
import time
import sys


if __name__ == "__main__":

    n = int(sys.argv[1])

    A = np.random.rand(n,n) 
    b = np.random.rand(n)

    start = time.perf_counter()
    np.linalg.solve(A,b)
    end = time.perf_counter()

    print("Python needed ",(end-start)," seconds.")


