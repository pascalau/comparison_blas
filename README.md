# Comparison of C++ and Python with Lapack/Blas for linear algebra

## Run

### Python

For the python programm simply start with `./solve.py n` where n is the rank of the system of equations. Dependancies are python3 and numpy.

### C++

The C++ part must first be compiled:
```
cd comparison_blas
mkdir build
cmake ..
make
```
Afterwards it can be executed in the build folder with `./solve n`

The build requirements are cmake and make aswell as a fast blas and lapack library to link.
